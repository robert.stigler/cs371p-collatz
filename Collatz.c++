// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s)
{
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j)
{
    int maxLength = 0;
    if (i <= j / 2)
    {
        i = (j / 2) + 1;
    }
    for (int test = i; test <= j; test++)
    {
        int testLength = get_cycle_length(test);
        if (testLength > maxLength)
        {
            maxLength = testLength;
        }
    }
    return maxLength;
}

// ----------------
// get_cycle_length
// ----------------

int get_cycle_length (int startingNumber)
{
    int cycleLength = 1;
    while (startingNumber != 1)
    {
        if (startingNumber % 2 == 0)
        {
            startingNumber = startingNumber / 2;
        }
        else
        {
            startingNumber = ((startingNumber * 3) + 1) / 2;
            ++cycleLength;
        }
        ++cycleLength;
    }
    return cycleLength;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v)
{
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w)
{
    string s;
    while (getline(r, s))
    {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
